console.clear();
// 4.find out the difference between strict and lose equality
// 4.1 === equality compare the type and the value 
// ------------------------------------------
console.log('=== strict equality')
var num = 0;
var obj = new String('0');
var str = '0';

console.log(num === obj);
console.log(num === str);
console.log(obj === str);
console.log(null === undefined);
console.log(obj === null);
console.log(obj === undefined);
console.log('' === null || [] === null);
console.log('' === undefined && [] === undefined);
// ------------------------------------------

// 4.2 == equality compares two values for equality after converting to a common type after that its same as ===
// ------------------------------------------
console.log('== lose equality')
var num = 0;
var obj = new String('0');
var str = '0';

console.log(num == obj);
console.log(num == str);
console.log(obj == str);
console.log(null == undefined);
console.log(obj == null);
console.log(obj == undefined);
console.log('' == null || [] == null);
console.log('' == undefined && [] == undefined);//this is because its an empty string

console.log('1,2' == [1, 2]);
// ------------------------------------------

// 4.3 special cases for both == and ===
// ------------------------------------------
console.log(NaN === NaN); // NaN is unequal to any other value including itself
console.log(+0 === -0);// treates them as same
// ------------------------------------------

// 4.4. why prefere === over == ? 
// bookmarked