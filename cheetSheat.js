// ----------------------------------------------------------------------------------------
// 1.0. undfined and null

/* null  represents the intentional absence of any object value indicating 
that a variable points to no object.
null is JavaScript's primitive values and is treated as falsy for boolean operations.
null is not an identifier for a property of the global object, like undefined 

Undefined is a property of the global object. That is, it is a variable in global scope. 
The initial value of undefined is the primitive value undefined.
A variable that has not been assigned a value is of type undefined. 
A method or statement also returns undefined 
if the variable that is being evaluated does not have an assigned value. 
A function returns undefined if a value was not returned.
Can also be used if the variable is declared */
// ------------------------------------------
let a = null;
let b;
console.log(a,b);
// ------------------------------------------

// 1.1 . difference between null and undefined
let dinosaur = undefined;
// You: What is dinosaur? (*)
// JavaScript: dinosaur? What's a dinosaur? I don't know what you're talking about. 
// You haven't ever mentioned any dinosaur before. 
// Are you seeing some other scripting language on the (client-)side?
let Aline = null;
// You: What is Aline?
// JavaScript: I don't know.
// In short; undefined is where no notion of the thing exists; it has no type, 
// and it's never been referenced before in that scope; 
// null is where the thing is known to exist, but it's not known what the value is.
// One thing to remember is that null is not, conceptually, 
// the same as false or "" or such, even if they equate after type casting

// 1.2 typeOf null and undefined 
// Every Object is derived from null value, 
// and therefore typeof operator returns object for it:
// ------------------------------------------
console.log(typeof null);          
console.log(typeof undefined);     
console.log(null === undefined);   
console.log(null == undefined);   
// ------------------------------------------
// ----------------------------------------------------------------------------------------
// 2.1 what is NAN when dose it appear
/* NaN is a property of the global object. In other words, it is a variable in global scope.
The initial value of NaN is Not-A-Number.NaN is a non-configurable, non-writable property. */
// ------------------------------------------
function sanitise(x) {
    if (isNaN(x)) {
        return NaN;
    }
    return x;
}
console.log(sanitise('1'));
console.log(sanitise('NotANumber'));
// ------------------------------------------

// 2.2  five different types of operations that return NaN:
// ------------------------------------------
console.log(parseInt("hi")); //Number cannot be parsed
console.log(Number(undefined));
console.log(Math.sqrt(-1)); //Math operation where the result is not a real number 
console.log(7 ** NaN); //Operand of an argument is NaN
console.log(0 * Infinity); // Indeterminate form
console.log(undefined + undefined)
console.log("hi" / 3) //Any operation that involves a string 
// and is not an addition operation 
// ------------------------------------------

// 2.3 isNaN vs Number.NaN
// ------------------------------------------
console.log('6.3 isNaN vs Number.NaN');
// ------------------------------------------
// return true if the value is currently NaN, or if it is going to be NaN 
// after it is coerced to a number
// ------------------------------------------
console.log(isNaN('hello world'));
console.log(isNaN(NaN));
console.log(isNaN('1'));
// isNaN(1n); TypeError: Conversion from 'BigInt' to 'number' is not allowed.
// ------------------------------------------
// return true only if the value is currently NaN:
// ------------------------------------------
console.log(Number.isNaN('hello world'));
console.log(Number.isNaN(NaN));
console.log(Number.isNaN("1"));
Number.isNaN(1n); 
// ------------------------------------------

// 2.4 some array methods cannot find NaN, while others can.
// ------------------------------------------
let arr = [2, 4, NaN, 12];
console.log(arr.indexOf(NaN));                      
console.log(arr.includes(NaN));                     
console.log(arr.findIndex(n => Number.isNaN(n)));   
// ------------------------------------------
// ----------------------------------------------------------------------------------------
// 3.0 what truthy and falsy values are 
// truthy - true in boolean context
// falsey - false in boolean context
// javascript uses coercion(similar to type conversation but implicit)

// 3.1 Truthy 
// ------------------------------------------
if (true) {
    console.log('Its truthy values ');
}
// ------------------------------------------
// what can be true   
// {}, [], 
// 42, -42, 12n, 3.14
// Infinity, -Infinity
// "0", "false"

// 3.2 The logical AND operator, && 
// ------------------------------------------
console.log(true && "dog");
console.log([] && "cat");
// ------------------------------------------

// 3.3 Falsy 
// ------------------------------------------
if (false) {
    console.log("you will never see me");
}
console.log("did you?");
// ------------------------------------------
//  what can be false 
// null, undefined, NaN
// "", '', `` 
// 0, -0, 0n, false 

// 3.4 The logical AND operator, && 
// ------------------------------------------
console.log(false && "dog");
console.log(0 && "cat");
// ------------------------------------------
// ----------------------------------------------------------------------------------------
// 3.0 what truthy and falsy values are 
// truthy - true in boolean context
// falsey - false in boolean context
// javascript uses coercion(similar to type conversation but implicit)

// 3.1 Truthy 
// ------------------------------------------
if (true) {
    console.log('Its truthy values ');
}
// ------------------------------------------
// what can be true   
// {}, [], 
// 42, -42, 12n, 3.14
// Infinity, -Infinity
// "0", "false"

// 3.2 The logical AND operator, && 
// ------------------------------------------
console.log(true && "dog");
console.log([] && "cat");
// ------------------------------------------

// 3.3 Falsy 
// ------------------------------------------
if (false) {
    console.log("you will never see me");
}
console.log("did you?");
// ------------------------------------------
//  what can be false 
// null, undefined, NaN
// "", '', `` 
// 0, -0, 0n, false 

// 3.4 The logical AND operator, && 
// ------------------------------------------
console.log(false && "dog");
console.log(0 && "cat");
// ------------------------------------------
// ----------------------------------------------------------------------------------------
// 4.find out the difference between strict and lose equality
// 4.1 === equality compare the type and the value 
// ------------------------------------------
console.log('=== strict equality')
var num = 0;
var obj = new String('0');
var str = '0';

console.log(num === obj);
console.log(num === str);
console.log(obj === str);
console.log(null === undefined);
console.log(obj === null);
console.log(obj === undefined);
console.log('' === null || [] === null);
console.log('' === undefined && [] === undefined);
// ------------------------------------------

// 4.2 == equality compares two values for equality after converting to a common type after that its same as ===
// ------------------------------------------
console.log('== lose equality')
var num = 0;
var obj = new String('0');
var str = '0';

console.log(num == obj);
console.log(num == str);
console.log(obj == str);
console.log(null == undefined);
console.log(obj == null);
console.log(obj == undefined);
console.log('' == null || [] == null);
console.log('' == undefined && [] == undefined);//this is because its an empty string

console.log('1,2' == [1, 2]);
// ------------------------------------------

// 4.3 special cases for both == and ===
// ------------------------------------------
console.log(NaN === NaN); // NaN is unequal to any other value including itself
console.log(+0 === -0);// treates them as same
// ------------------------------------------

// 4.4. why prefere === over == ? 
// bookmarked
// ----------------------------------------------------------------------------------------
// 5.0. find out what is callby refernce and call by value in javascript 

// Value type or primitives- number,string,boolean,symbol,undefined,null
// Reference types- object function array 

// call by value create a copy ,call by reference passes the memory location  
// Javascript has 2 types - value, reference type

// 5.1. value type 
// ------------------------------------------
let x = 10;
let y = x;
x = 20;
console.log(x, y)
//they are independent

//behaviour in function
let numb = 10;
function inc(count) {  // called a Formal Argument
    count += 5;
}
inc(numb);
console.log(numb) //called actual argument
// ------------------------------------------


// 5.2. reference types 
// C and C++ we use the address of ("&") operator and in C# 'ref' to send the address of a variable. 
// In JavaScript, the situation is a little different. 
// We need to send the actual object as a function argument to send the reference of an object
//bcz object points to the memory location
// ------------------------------------------
let x1 = { value: 10 };
let y1 = x1; // 10
x1.value = 20;
console.log(x1, y1)

//behaviour in function
let obj1 = { num: 10 };
function inc(objpass) {
    objpass.num += 5;
}
inc(obj1);
console.log(obj1)
// ------------------------------------------
// ----------------------------------------------------------------------------------------
// 6.0. find out how to use the spread operator

/* ... (spread oper) allows an iterable (arr,str) to be expanded in place of zero or more 
args (for function call) or elemets (for arr literals) or an 
objetc expression with zero or more key-value pairs  */

// 6.1
// spread syntax can take any length of arr but we need to add more args to function 
// ------------------------------------------
function sum(b, x, y, z, w) {
    return b + x + y + z + w;
}
const arry1 = [1, 2, 3,];
let boost = 100;
console.log(sum(boost, ...arry1, 1000));
// ------------------------------------------


// 6.2 to add new elements to the arr
// it is a pass by value syntax and can be used to add(concate) or create new arr
// ------------------------------------------
let numberArr = [0, 1, 2];
let newele = 12;
let newArr = [15, 20]
numberArr = [...numberArr, newele, ...newArr];
console.log(numberArr);
// ------------------------------------------

// 6.3 convert string char into arr
// ------------------------------------------
let greet = "Hello"
let all = "world"
greetArr = [...greet, ...all];
console.log(greetArr);
// ------------------------------------------

// 6.4 Apply for new operator
/* When calling a constructor with new it's not possible to directly use an array and apply() 
(apply() does a [[Call]] and not a [[Construct]]). 
However, an array can be easily used with new thanks to spread syntax: */
// ------------------------------------------
let customDate = [2001, 10, 12];  // 1 Jan 1970
let d = new Date(...customDate);
console.log(d);
// ------------------------------------------

// 6.5 Spread in object literals
// ------------------------------------------
let objex1 = { foo: 'bar', x: 42 };
let objex2 = { foo: 'baz', y: 13 };
let clonedObj = { ...objex1 };
let mergedObj = { ...objex1, ...objex2 };

console.log(clonedObj);
console.log(mergedObj);

const merge = (...objects) => ({ ...objects });
console.log(merge(objex1, objex2));
console.log(merge({}, objex1, objex2));
// ------------------------------------------