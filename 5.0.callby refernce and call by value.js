console.clear();
// 5.0. find out what is callby refernce and call by value in javascript 

// Value type or primitives- number,string,boolean,symbol,undefined,null
// Reference types- object function array 

// call by value create a copy ,call by reference passes the memory location  
// Javascript has 2 types - value, reference type

// 5.1. value type 
// ------------------------------------------
let x = 10;
let y = x;
x = 20;
console.log(x, y)
//they are independent

//behaviour in function
let numb = 10;
function inc(count) {  // called a Formal Argument
    count += 5;
}
inc(numb);
console.log(numb) //called actual argument
// ------------------------------------------


// 5.2. reference types 
// C and C++ we use the address of ("&") operator and in C# 'ref' to send the address of a variable. 
// In JavaScript, the situation is a little different. 
// We need to send the actual object as a function argument to send the reference of an object
//bcz object points to the memory location
// ------------------------------------------
let x1 = { value: 10 };
let y1 = x1; // 10
x1.value = 20;
console.log(x1, y1)

//behaviour in function
let obj1 = { num: 10 };
function inc(objpass) {
    objpass.num += 5;
}
inc(obj1);
console.log(obj1)
