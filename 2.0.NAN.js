console.clear();
// 2.1 what is NAN when dose it appear
/* NaN is a property of the global object. In other words, it is a variable in global scope.
The initial value of NaN is Not-A-Number.NaN is a non-configurable, non-writable property. */
// ------------------------------------------
function sanitise(x) {
    if (isNaN(x)) {
        return NaN;
    }
    return x;
}
console.log(sanitise('1'));
console.log(sanitise('NotANumber'));
// ------------------------------------------

// 2.2  five different types of operations that return NaN:
// ------------------------------------------
console.log(parseInt("hi")); //Number cannot be parsed
console.log(Number(undefined));
console.log(Math.sqrt(-1)); //Math operation where the result is not a real number 
console.log(7 ** NaN); //Operand of an argument is NaN
console.log(0 * Infinity); // Indeterminate form
console.log(undefined + undefined)
console.log("hi" / 3) //Any operation that involves a string 
// and is not an addition operation 
// ------------------------------------------

// 2.3 isNaN vs Number.NaN
// ------------------------------------------
console.log('6.3 isNaN vs Number.NaN');
// ------------------------------------------
// return true if the value is currently NaN, or if it is going to be NaN 
// after it is coerced to a number
// ------------------------------------------
console.log(isNaN('hello world'));
console.log(isNaN(NaN));
console.log(isNaN('1'));
// isNaN(1n); TypeError: Conversion from 'BigInt' to 'number' is not allowed.
// ------------------------------------------
// return true only if the value is currently NaN:
// ------------------------------------------
console.log(Number.isNaN('hello world'));
console.log(Number.isNaN(NaN));
console.log(Number.isNaN("1"));
Number.isNaN(1n); 
// ------------------------------------------

// 2.4 some array methods cannot find NaN, while others can.
// ------------------------------------------
let arr = [2, 4, NaN, 12];
console.log(arr.indexOf(NaN));                      
console.log(arr.includes(NaN));                     
console.log(arr.findIndex(n => Number.isNaN(n)));   
// ------------------------------------------
