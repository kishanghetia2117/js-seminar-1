console.clear();
// 1.0. undfined and null

/* null  represents the intentional absence of any object value indicating 
that a variable points to no object.
null is JavaScript's primitive values and is treated as falsy for boolean operations.
null is not an identifier for a property of the global object, like undefined 

Undefined is a property of the global object. That is, it is a variable in global scope. 
The initial value of undefined is the primitive value undefined.
A variable that has not been assigned a value is of type undefined. 
A method or statement also returns undefined 
if the variable that is being evaluated does not have an assigned value. 
A function returns undefined if a value was not returned.
Can also be used if the variable is declared */
// ------------------------------------------
let a = null;
let b;
console.log(a,b);
// ------------------------------------------

// 1.1 . difference between null and undefined
let dinosaur = undefined;
// You: What is dinosaur? (*)
// JavaScript: dinosaur? What's a dinosaur? I don't know what you're talking about. 
// You haven't ever mentioned any dinosaur before. 
// Are you seeing some other scripting language on the (client-)side?
let Aline = null;
// You: What is Aline?
// JavaScript: I don't know.
// In short; undefined is where no notion of the thing exists; it has no type, 
// and it's never been referenced before in that scope; 
// null is where the thing is known to exist, but it's not known what the value is.
// One thing to remember is that null is not, conceptually, 
// the same as false or "" or such, even if they equate after type casting

// 1.2 typeOf null and undefined 
// Every Object is derived from null value, 
// and therefore typeof operator returns object for it:
// ------------------------------------------
console.log(typeof null);          
console.log(typeof undefined);     
console.log(null === undefined);   
console.log(null  == undefined);   
// ------------------------------------------

