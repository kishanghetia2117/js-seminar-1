console.clear();
// 3.0 what truthy and falsy values are 
// truthy - true in boolean context
// falsey - false in boolean context
// javascript uses coercion(similar to type conversation but implicit)

// 3.1 Truthy 
// ------------------------------------------
if (true) {
    console.log('Its truthy values ');
}
// ------------------------------------------
// what can be true   
// {}, [], 
// 42, -42, 12n, 3.14
// Infinity, -Infinity
// "0", "false"

// 3.2 The logical AND operator, && 
// ------------------------------------------
console.log(true && "dog");
console.log([] && "cat");
// ------------------------------------------

// 3.3 Falsy 
// ------------------------------------------
if (false) {
    console.log("you will never see me");
}
console.log("did you?");
// ------------------------------------------
//  what can be false 
// null, undefined, NaN
// "", '', `` 
// 0, -0, 0n, false 

// 3.4 The logical AND operator, && 
// ------------------------------------------
console.log(false && "dog");
console.log(0 && "cat");
// ------------------------------------------
