console.clear()
// 6.0. find out how to use the spread operator

/* ... (spread oper) allows an iterable (arr,str) to be expanded in place of zero or more 
args (for function call) or elemets (for arr literals) or an 
objetc expression with zero or more key-value pairs  */

// 6.1
// spread syntax can take any length of arr but we need to add more args to function 
// ------------------------------------------
function sum(b, x, y, z, w) {
    return b + x + y + z + w;
}
const arry1 = [1, 2, 3,];
let boost = 100;
console.log(sum(boost, ...arry1, 1000));
// ------------------------------------------


// 6.2 to add new elements to the arr
// it is a pass by value syntax and can be used to add(concate) or create new arr
// ------------------------------------------
let numberArr = [0, 1, 2];
let newele = 12;
let newArr = [15, 20]
numberArr = [...numberArr, newele, ...newArr];
console.log(numberArr);
// ------------------------------------------

// 6.3 convert string char into arr
// ------------------------------------------
let greet = "Hello"
let all = "world"
greetArr = [...greet, ...all];
console.log(greetArr);
// ------------------------------------------

// 6.4 Apply for new operator
/* When calling a constructor with new it's not possible to directly use an array and apply() 
(apply() does a [[Call]] and not a [[Construct]]). 
However, an array can be easily used with new thanks to spread syntax: */
// ------------------------------------------
let customDate = [2001, 10, 12];  // 1 Jan 1970
let d = new Date(...customDate);
console.log(d);
// ------------------------------------------

// 6.5 Spread in object literals
// ------------------------------------------
let objex1 = { foo: 'bar', x: 42 };
let objex2 = { foo: 'baz', y: 13 };
let clonedObj = { ...objex1 };
let mergedObj = { ...objex1, ...objex2 };

console.log(clonedObj);
console.log(mergedObj);

const merge = (...objects) => ({ ...objects });
console.log(merge(objex1, objex2));
console.log(merge({}, objex1, objex2));
// ------------------------------------------

const publicIpdataset = [{ "id": 1, "ip_address": "253.171.63.171" }, { "id": 2, "ip_address": "50.231.58.150" },]


// 2. Split their IP address into their components eg. 111.139.161.143 has components 111 139 161 143.

// const confIp = publicIpdataset.map((ipInfo) => {
//     ipInfo.ip_address = ipInfo.ip_address.split('.').join(' ');
//     return ipInfo
// });
// console.log(confIp)
// console.log(publicIpdataset)


// const confIp = (dataset) => {
//     temp = [dataset].map((ipInfo) => {
//         console.log(ipInfo)
//         console.log()
//         ipInfo.ip_address = ipInfo.ip_address.split('.').join(' ');
//         return ipInfo
//     });
//     return temp
// }

// console.log(confIp(...publicIpdataset));
// console.log();
// console.log(publicIpdataset);


// const confIp = (...dataset) => {
//     temp = dataset.map((ipInfo) => { // turn it into dataset[0]
//         console.log(ipInfo)
//         console.log("endshere")
//         // ipInfo.ip_address = ipInfo.ip_address.split('.').join(' ');
//         return ipInfo
//     });
//     return temp
// }
// console.log(confIp(publicIpdataset));
// console.log();
// console.log(publicIpdataset);


// const confIp = (dataset) => {
//     tmp = [...dataset].map((ipInfo) => {
//         console.log(ipInfo)
//         ipInfo.ip_address = ipInfo.ip_address.split('.').join(' ');
//         return ipInfo
//     });
//     return tmp
// }
// console.log(confIp(publicIpdataset));
// console.log();
// console.log(publicIpdataset);

// correct method ---------------------------------------------------------------------------------------
// const confIp = (dataset) => {
//     tmp = dataset.map((ipInfo) => {
//         let cloneObj = { ...ipInfo };
//         cloneObj.ip_address = cloneObj.ip_address.split('.').join(' ');
//         return cloneObj
//     });
//     return tmp
// }
// console.log(confIp(publicIpdataset));
// console.log();
// console.log(publicIpdataset);